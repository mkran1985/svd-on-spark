Readme File
Author: Muthukumaran Chandrasekaran
CSCI 6900: Mining Massive Datasets
Assignment 2 - SVD on Spark 
-----------------------------------

Specs:
-------
Programming Language: Scala
OS: Windows (laptop)
IDE: IntelliJ Idea
Scala Version: 2.10.5
Spark Version: 1.2.1
Jar: svd-build.jar (Path: svd\out\artifacts\svd_build_jar)
Arguments: <Path\to\images\directory> <k>

Command:
---------
I used the IDE to run my code. 
But I think if you run the following from command line it will work:

spark-submit svd-build.jar <Path\to\images\directory> <k>

What's not done:
-----------------
Question 4
Bonus Question 1 and 2

References:
------------
Will helped me set up maven for setting up Spark
Roi talked to me for about 5 min about his general methodology
Roi's and Baha's emails to the listserv helped a lot
Baha helped me with some scala syntax 
Baha and I used the white board to discuss Question 3 and the Bonus Question 3

Deliverables:
--------------
Project files are committed/pushed to https://bitbucket.org/mkran1985/assignment2

Question 1:
-----------
--master local 
Log file: log-10.txt      //k=10, no. of cores = 1
Runtime : 567.900064 s

--master local[*]
Log file: log-10-all.txt  //k=10, no. of cores = 4
Runtime : 275.431716 s

Yes. There is a change in runtime.
We could say that as the no. of cores increases, the runtime decreases proportionally. However, there is an overhead for initializing the cores.
Yes. I expect that if we used multiple cores across multiple machines, barring the overhead of initializing the cores, there will be savings. But again, the locality of the machines (communication over the network) may play an important role in determining the runtime. 

Question 2:
-----------
k=10, no. of cores = * (i.e., 4)
Reconstructed Image without svd step : average-original-yeast-ribosome-10.png	
Reconstructed Image with svd step    : average-yeast-ribosome-10.png
Log File       : log-10-all-fNorm.txt
Frobenius Norm : 240.22378036042065

k=50, no. of cores = * (i.e., 4)
Reconstructed Image without svd step : average-original-yeast-ribosome-50.png	
Reconstructed Image with svd step    : average-yeast-ribosome-50.png
Log File       : log-50-all-fNorm.txt
Frobenius Norm : 151.16086151792624

k=100, no. of cores = * (i.e., 4)
Reconstructed Image without svd step : average-original-yeast-ribosome-100.png	
Reconstructed Image with svd step    : average-yeast-ribosome-100.png
Log File       : log-100-all-fNorm.txt
Frobenius Norm : 97.81990424337341

As k approaches 420, the reconstruction error approaches 0. 
Ideal choice for k: This depends on what your tolerance for the reconstruction error is. Based on this tolerance % we can compute a minimum value of k that can achieve this level of tolerance by leveraging the fact that the "s", the diagonal matrix(or vector), is ordered. 

Question 3:
-----------
Top 10 pixels (row,col): (72,8), (379,112), (67,5), (0,400), (41,50), (412,171), (242,104), (160,64), (365,24), (195,286)
Log File: log-q3-top10.txt

Question 4:
-----------
Skipped, Sorry!

Bonus Question 6.1:
-------------------
Skipped, Sorry!

Bonus Question 6.2:
-------------------
Skipped, Sorry!

Bonus Question 6.3: Spark Style 
-------------------
Say,
Affinity Matrix A
Diagonal Matrix D
Laplacian L = D^{-1/2} * A * D^{-1/2}
i -> row index
j -> col index
Ai -> ith row vector of A
dii -> ith row & ith col of (diagonal matrix) D
d = vector of diagonal elements of D
di -> ith element of vector d
** -> element-wise multiplication of two vectors

Mapper Pseudo-code:
Input <K,V> = <i,Ai>
Output <K,V> = <i, ((Ai ** 1/d^{1/2}) * (1/di^{1/2}))>












