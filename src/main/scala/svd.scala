import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import breeze.linalg._
import breeze.numerics.sqrt
import org.apache.spark.SparkContext


/**
 * Created by Muthu on 3/19/2015.
 */

object svd{
  def constructDenseMatrix(bufferedImage: BufferedImage): DenseMatrix[Double] ={
    val h = bufferedImage.getHeight
    val w = bufferedImage.getWidth
    val dMat = DenseMatrix.zeros[Double](w,h)
    for(x<-0 until w;y<-0 until h) {
      dMat(y, x) = bufferedImage.getRGB(x, y) & 0x000000FF
    }
    dMat
  }
  def computeSVD(dMat:DenseMatrix[Double], k:Int): DenseMatrix[Double] ={
    val breeze.linalg.svd.SVD(u,s,vt) = breeze.linalg.svd(dMat)
    u(::,0 until k) * diag(s(0 until k)) * vt(0 until k,::)
  }

  def writeImage(dMat:DenseMatrix[Double], path:String){
    val (w,h) = (dMat.cols,dMat.rows)
    val img = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB)
    for(x <- 0 until w; y <- 0 until h){
      var pix = dMat(y,x).toInt
      img.setRGB(x,y ,0xff000000 | pix<<16 | pix<<8 | pix)
    }
    ImageIO.write(img,"png",new File(path))
  }

  def computeMinMax(dMat1:(DenseMatrix[Double],DenseMatrix[Double]),dMat2:(DenseMatrix[Double],DenseMatrix[Double])): (DenseMatrix[Double],DenseMatrix[Double]) ={
    val (w,h) = (dMat1._1.cols,dMat1._1.rows)
    val minMat = DenseMatrix.zeros[Double](h,w)
    val maxMat = DenseMatrix.zeros[Double](h,w)

    for(x <- 0 until w; y <- 0 until h){
      minMat(x,y) = math.min(dMat1._1(x,y), dMat2._1(x,y))
      maxMat(x,y) = math.max(dMat1._2(x,y), dMat2._2(x,y))
    }

    (minMat,maxMat)
  }

  def main (args: Array[String]):Unit = {
    val k = args(1).toInt
    val images = (new File(args(0))).listFiles
    val len = images.length.toDouble
    val sc = new SparkContext("local[*]","")

//    Uncomment below if you want to run SVD (Questions 1 & 2)
//    ----------------------------------------------------------
//    val avgImage = sc.parallelize(images).map(image => ImageIO.read(image)).map(constructDenseMatrix).map(computeSVD(_,k)).reduce(_+_)/len
//    val avgOrigImage = sc.parallelize(images).map(image => ImageIO.read(image)).map(constructDenseMatrix).reduce(_+_)/len
//    writeImage(avgImage,"average-yeast-ribosome-"+args(1)+".png")
//    writeImage(avgOrigImage,"average-original-yeast-ribosome-"+args(1)+".png")
//    val diff = avgOrigImage - avgImage
//    println(sqrt(trace(diff*diff.t)))
//    ----------------------------------------------------------

//    Question 3
    val (minM,maxM) = sc.parallelize(images).map(image => ImageIO.read(image)).map(constructDenseMatrix).map(img => (img,img)).reduce(computeMinMax)
    val spanM = maxM - minM
    println(argtopk(spanM,10))
  }
}
