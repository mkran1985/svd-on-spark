/**
 * Created by Muthu on 4/9/2015.
 */
import breeze.linalg.SparseVector
import org.apache.spark.SparkContext
import org.apache.spark.mllib.clustering.LDA

import scala.io.Source


object lda{

  // Taken from https://github.com/apache/spark/blob/master/mllib/src/main/scala/org/apache/spark/mllib/linalg/Vectors.scala
  def breeze2mllibSpVec(spVec:SparseVector[Double] ):org.apache.spark.mllib.linalg.Vector = {
    if (spVec.index.length == spVec.used) {
      new org.apache.spark.mllib.linalg.SparseVector(spVec.length, spVec.index, spVec.data)
    }else {
      new org.apache.spark.mllib.linalg.SparseVector(spVec.length, spVec.index.slice(0, spVec.used), spVec.data.slice(0, spVec.used))
    }
  }

  def main (args: Array[String]){

    val sc = new SparkContext("local[*]","")
//    val file = sc.textFile("twitter\\part-00000")
//    val file = sc.textFile("C:\\Users\\Muthu\\IdeaProjects\\lda\\twitter\\utweet")
    val file = sc.textFile(args(0))

    val tokens = file.flatMap(_.split("\t")(1).split(" ").map(_.toLowerCase()))
    val distinctTokens = tokens.distinct()
    val tokenUniqueCount = distinctTokens.count()

    //QUESTION #1
    println(tokenUniqueCount)

    val c = distinctTokens.count.toInt
    val token4LDA = distinctTokens.zipWithIndex
    val temp = file.flatMap(row => {row.split("\t")(1).split(" ").zip(Stream.continually(row.split("\t")(0).toLong))}).join(token4LDA).map(skipToken => skipToken._2).groupByKey().map(blah => {
        val spVec = SparseVector.zeros[Double](c);
        for( blahh <- blah._2) {
            spVec(blahh.toInt)=spVec(blahh.toInt)+1;
        }
        (blah._1,breeze2mllibSpVec(spVec));
    });

    //QUESTION #2
    //taken from https://spark.apache.org/docs/latest/mllib-clustering.html#latent-dirichlet-allocation-lda

    val ldaModel = new LDA().setK(10).run(temp)
//    val topics = ldaModel.topicsMatrix

//    to print weights
//    for (topic <- Range(0, 10)) {
//      print("Topic " + topic + ":")
//      for (word <- Range(0, ldaModel.vocabSize)) { print(" " + topics(word, topic)); }
//      println()
//    }

//    To get top 1 word corresponding to max weight for each topic
//    val topWords = ldaModel.describeTopics(1)
    val indxToToken = token4LDA.map(_.swap).collectAsMap()
    for ( topTuple <- ldaModel.describeTopics(1) ) {
      for ( wordLabel <- topTuple._1) {
        println( indxToToken.get( wordLabel ) )
      }
    }


    //QUESTION #3
    val source = Source.fromFile(args(0))
    val stopFile = sc.textFile(args(1));
    val stopTokens = stopFile.flatMap(_.split(",")).collect()

    //QUESTION #4
    //remove stop words and then run the LDA part of the code (above) again!

    //Option 1: Use the Stanford Topic Modeling Toolbox: http://nlp.stanford.edu/software/tmt/tmt-0.4/
//    TermStopListFilter(stopTokens.toList)

    //Option 2: use filter(...)
    val stopTokensSet = stopTokens.toSet
    val file2 = sc.textFile(args(0))
    val tokens2 = file2.flatMap(_.split("\t")(1).split(" ").map(_.toLowerCase())).filter( word => !stopTokensSet.contains(word))
    val distinctTokens2 = tokens2.distinct()
    val tokenUniqueCount2 = distinctTokens2.count()
    println(tokenUniqueCount2)

    val c2 = distinctTokens2.count.toInt
    val token4LDA2 = distinctTokens2.zipWithIndex
    val temp2 = file2.flatMap(row => {row.split("\t")(1).split(" ").zip(Stream.continually(row.split("\t")(0).toLong))}).join(token4LDA2).map(skipToken => skipToken._2).groupByKey().map(blah => {
      val spVec = SparseVector.zeros[Double](c);
      for( blahh <- blah._2) {
        spVec(blahh.toInt)=spVec(blahh.toInt)+1;
      }
      (blah._1,breeze2mllibSpVec(spVec));
    });

    val ldaModel2 = new LDA().setK(10).run(temp2)

    val indxToToken2 = token4LDA2.map(_.swap).collectAsMap()
    for ( topTuple <- ldaModel2.describeTopics(1) ) {
      for ( wordLabel <- topTuple._1) {
        println( indxToToken2.get( wordLabel ) )
      }
    }

  }
}

